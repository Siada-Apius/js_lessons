/**
 * Created by Siada on 14.12.2015.
 */

//Array.prototype.new_some = function(func) {
//    if (typeof func != 'function') {
//        console.log('parameter must be a function');
//
//        return false;
//    }
//
//    for (var i = 0; i < this.length; i++)
//    {
//        //arguments - isPositive(number)
//        if (func.call(arguments, this[i])) {
//            return true;
//        }
//    }
//    return false;
//};

function isPositive(number) {
    return number > 0;
}
function mirror(element) {
    return element * -1;
}
function sum(previousValue, currentValue, index, array) {
    return previousValue + currentValue;
}


function some(arr, func) {
    for (var i = 0; i < arr.length; i++)
    {
        if (func.call(func, arr[i])) {
            return true;
        }
    }
    return false;
}

function every(arr, func) {
    for (var i = 0; i < arguments[0].length; i++)
    {
        if ( ! arguments[1].call(arguments[1], arguments[0][i])) {
            return false;
        }
    }
    return true;
}

function map(arr, func) {

    var result = [];

    for (var i = 0; i < arr.length; i++) {
        result.push(func.call(func, arr[i]));
    }

    return result;
}

function reduce (arr, func) {
    var result;

    for (var i = 0; i < arr.length; i++) {

        result = func.call(func, arr[i], arr[++i], arr);
    }

    return result;
}

var arr = [1, -1, 2, -2, 3, 4];
console.log(arr);
console.log(some(arr, isPositive));
console.log(every(arr, isPositive));
console.log(map(arr, mirror));
console.log(reduce(arr, sum));