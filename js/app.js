var result = function (name, res) {
   return console.log('method: '+ name + '\n' + 'result: ', res)
};

var get_random_int = function (min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
};

var rand = function (d) {
    var all = typeof d === 'string' ? d.split(' ') : d;
    var range = get_random_int(0, all.length);
    var less = all.length / 2;
    var from = get_random_int(0, less);
    var to = get_random_int(less, all.length);

    return {
        all: all,
        range: range,
        less: less,
        from: from,
        to: to
    };
};

var rand_array = function (d) {
    var array = ['Emil', 'Tobias', 'Linus', 'Lemon', 'Kiwi'];
    var random_array = array[get_random_int(0, array.length)];
    var symbols = ['!', '@', ',', '.', ':'];
    var random_symbol = symbols[get_random_int(0, symbols.length)];
    var range = get_random_int(1, d.length);

    return {
        array: array,
        random_array: random_array,
        symbols: symbols,
        random_symbol: random_symbol,
        range: range
    };

};

//string
var index_of = function (d, random) {
    result(d.indexOf.name, d.indexOf(random.all[random.range]));
};
var last_index_of = function (d, random) {
    result(d.lastIndexOf.name, d.lastIndexOf(random.all[random.range]));
};
var srch = function (d, random) {
    result(d.search.name, d.search(random.all[random.range]));
};
var slce = function (d, random) {
    result(d.slice.name, d.slice(random.from, random.to));
};
var substr = function (d, random) {
    result(d.substring.name, d.substring(random.from, random.to));
};
var replc = function (d, random) {
    result(d.replace.name, d.replace(random.all[random.from], random.all[random.to]));
};
var to_upper_case = function (d, random) {
    result(d.toUpperCase.name, d.toUpperCase());
};
var to_lower_case = function (d, random) {
    result(d.toLowerCase.name, d.toLowerCase());
};
var cnct = function (d, random) {
    result(d.concat.name, d.concat('!!!'));
};
var char_at = function (d, random) {
    result(d.charAt.name, d.charAt(random.range));
};
var char_code_at = function (d, random) {
    result(d.charCodeAt.name, d.charCodeAt(random.range));
};
var splt = function (d, random) {
    result(d.split.name, d.split(' '));
};

//number
var numb = function (d, random) {
    result(Number.name, Number(d));
};
var parse_float = function (d, random) {
    result(parseFloat.name, parseFloat(d.toString()));
};
var parse_int = function (d, random) {
    result(parseInt.name, parseInt(d.toString()));
};
var value_of = function (d, random) {
    result(valueOf.name, d.valueOf());
};
var num_to_string = function (d, random) {
    result(toString.name, d.toString());
};
var to_exponential = function (d, random) {
    result(d.toExponential.name, d.toExponential(random.range));
};
var to_fixed = function (d, random) {
    result(d.toFixed.name, d.toFixed(random.range));
};
var to_precision = function (d, random) {
    result(d.toPrecision.name, d.toPrecision(d.length));
};

//array
var create_new_array = function(array) {
    var new_array = [];

    for (i = 0; i < array.length; i++) {
        new_array.push(array[i]);
    }

    return new_array;
};

var to_string = function(d, random) {
    result(create_new_array(d).toString.name, create_new_array(d).toString());
};
var jin = function(d, random) {
    result(create_new_array(d).join.name, create_new_array(d).join(random.random_symbol));
};
var psh = function(d, random) {
    result(create_new_array(d).push.name, create_new_array(d).push(random.random_array));
};
var pOp = function(d, random) {
    result(create_new_array(d).pop.name, create_new_array(d).pop());
};
var shft = function(d, random) {
    result(create_new_array(d).shift.name, create_new_array(d).shift());
};
var un_shift = function(d, random) {
    result(create_new_array(d).unshift.name, create_new_array(d).unshift(random.random_array));
};
var elemnt = function(d, random) {
    result('['+ random.range +']', create_new_array(d)[random.range] = random.random_array);
};
var del = function(d, random) {
    result('delete', delete create_new_array(d)[random.range]);
};
var splic = function(d, random) {
    var slice_result = create_new_array(d);
    slice_result.splice(random.range, random.range, random.random_array);

    result(create_new_array(d).splice.name, slice_result);
};
var srt = function(d, random) {
    result(create_new_array(d).sort.name, create_new_array(d).sort());
};
var revers = function(d, random) {
    result(create_new_array(d).reverse.name, create_new_array(d).reverse());
};
var conct = function(d, random) {
    result(create_new_array(d).concat.name, create_new_array(d).concat(random.array));
};
var array_slce = function(d, random) {
    result(create_new_array(d).slice.name, create_new_array(d).slice(random.range));
};
var array_value_of = function(d, random) {
    result(create_new_array(d).valueOf.name, create_new_array(d).valueOf());
};


function type_check(data) {
    console.log('param: ' + data + '\n\n');

    if (typeof data === 'string') {

        if (data.indexOf(' ') < 0) {
            console.log('String must contain at least two words!');

            return false;
        }

        index_of(data, rand(data));
        last_index_of(data, rand(data));
        srch(data, rand(data));
        slce(data, rand(data));
        substr(data, rand(data));
        replc(data, rand(data));
        to_upper_case(data, rand(data));
        to_lower_case(data, rand(data));
        cnct(data, rand(data));
        char_at(data, rand(data));
        char_code_at(data, rand(data));
        splt(data, rand(data));

    } else if (typeof data === 'number') {

        numb(data, rand(data));
        parse_float(data, rand(data));
        parse_int(data, rand(data));
        value_of(data, rand(data));
        num_to_string(data, rand(data));
        to_exponential(data, rand(data));
        to_fixed(data, rand(data));
        to_precision(data, rand(data));

    } else if (typeof data == 'object' && data instanceof Array) {

        if (data.length < 2) {
            console.log('Array must contain at least two values!');

            return false;
        }

        to_string(data, rand_array(data));
        psh(data, rand_array(data));
        jin(data, rand_array(data));
        pOp(data, rand_array(data));
        shft(data, rand_array(data));
        un_shift(data, rand_array(data));
        elemnt(data, rand_array(data));
        del(data, rand_array(data));
        splic(data, rand_array(data));
        srt(data, rand_array(data));
        revers(data, rand_array(data));
        conct(data, rand_array(data));
        array_slce(data, rand_array(data));
        array_value_of(data, rand_array(data));

    } else {
        console.log('Wrong type');

        return false;
    }
}


var str = 'Start day - hello world, end day - goodbye world';
var num = 123.453;
var arr = ['red', 'blue', 'green'];

type_check(arr);

